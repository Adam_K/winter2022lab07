public class Board{
	
	private final int maxRoll = 3;
	
	private Die firstRoll;
	private Die secondRoll;
	private int[][] closedTiles;
	
	
	public Board(){
		this.firstRoll = new Die();
		this.secondRoll = new Die();
		this.closedTiles = new int[6][6];
	}
		
	
	public String toString(){
		String builder = "";
		 for (int i = 0; i < this.closedTiles.length; i++){
			for (int j = 0; j < this.closedTiles[i].length; j++){
				builder += this.closedTiles[i][j] + ",";
			}
			builder+="\n";
		 }
		 return builder;
	}
		 
	
	public boolean playATurn(){
		
		this.firstRoll.roll();
		this.secondRoll.roll();
		
		System.out.println(firstRoll);
		System.out.println(secondRoll);
		
			
		
		this.closedTiles[this.firstRoll.getPips()-1][this.secondRoll.getPips()-1]++;
		
		if(this.closedTiles[firstRoll.getPips()-1][secondRoll.getPips()-1]  < maxRoll){	
			System.out.println("Closing tile");
			return false;
		}
		
		else{
			System.out.println("Tile is already shut");
			return true;
		}
		
	}
	
	
	
}
	

	

	
