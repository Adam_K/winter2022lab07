public class ShutTheBox{
	
	public static void main(String[] args){
		System.out.println("Welcome to Shut the Box");
		
		boolean gameOver = false;
	    Board board = new Board();
	
		while(gameOver == false){
		System.out.println("Player's 1 turn");
		System.out.println(board);
		
		if(board.playATurn() == true){
		System.out.println("Player 2 wins");
		gameOver = true;
		
		//Break out of the loop so the program end.
		break;
		}
			
		else{
		System.out.println("Player's 2 turn");
		System.out.println(board);
		}
			
		if(board.playATurn() == true){
		System.out.println("Player 1 wins");
		gameOver = true;
		break;
		}
		
	  }
	  
	}

}
